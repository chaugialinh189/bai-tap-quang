﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thuthapquang
{
    internal class Program
    {
        static void Main(string[] args)
        {            
            Console.WriteLine("chuong trinh tinh so dong xu vang tu viec thu thap quang ");
            Console.WriteLine("nhap so manh quang da thu thap: ");
            int quantity = int.Parse(Console.ReadLine());
            int ten = Math.Min(quantity,10) * 10;
            quantity = Math.Max(quantity - 10, 0) * Math.Min(quantity, 1);
            int five = Math.Min(quantity, 5) * 5;
            quantity = Math.Max(quantity - 5 , 0) * Math.Min(quantity,1);
            int three = Math.Min(quantity, 3) * 2;
            quantity = Math.Max(quantity - 3, 0) * Math.Min(quantity,1);
            int remainder = quantity;
            int sum = ten + five + three + remainder;
            Console.WriteLine("so luong dong xu ma ban nhat duoc la: " + sum + " dong xu vang" );

        }
    }
}
